<?php

namespace App\Services\Image;

use Illuminate\Http\Request;

/**
 * Interface ImageWorker
 * @package App\Services\Image
 */
interface ImageWorker
{
    /**
     * @param Request $request
     * @return string
     */
    public function resizeAndSave(Request $request): string;

    /**
     * @param string $path
     */
    public function delete(string $path): void;

    /**
     * @param Request $request
     * @param string $oldPath
     * @return string
     */
    public function update(Request $request, string $oldPath): string;
}
