<?php

namespace App\Services\Image;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

/**
 * Class ImageInStorage
 * @package App\Services\Image
 */
class ImageInStorage implements ImageWorker
{
    const PATH = 'app/images/products';

    /**
     * @param Request $request
     * @return string
     */
    public function resizeAndSave(Request $request): string
    {
        $image = $request->file('image');
        $imageName = time().'.'.$image->extension();

        $destinationPath = storage_path(self::PATH);

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 666, true);
        }

        $img = Image::make($image->path());

        $img->resize(320, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$imageName);

        return self::PATH . '/' . $imageName;
    }

    /**
     * @param string $path
     */
    public function delete(string $path): void
    {
        if(file_exists(storage_path($path))){
            unlink(storage_path($path));
        }
    }

    /**
     * @param Request $request
     * @param string $oldPath
     * @return string
     */
    public function update(Request $request, string $oldPath): string
    {
        $this->delete($oldPath);
        return $this->resizeAndSave($request);
    }
}
