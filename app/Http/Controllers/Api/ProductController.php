<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Services\Image\ImageInStorage;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class ProductController
 * @package App\Http\Controllers\Api
 */
class ProductController extends Controller
{
    /**
     * @var ImageInStorage
     */
    private ImageInStorage $imageInStorage;

    /**
     * ProductController constructor.
     * @param ImageInStorage $imageInStorage
     */
    public function __construct(
        ImageInStorage $imageInStorage
    ) {
        $this->imageInStorage = $imageInStorage;
    }

    /**
     * @param CreateProductRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function create(CreateProductRequest $request): JsonResponse
    {
        $imageName = $this->imageInStorage->resizeAndSave($request);

        $product = Product::create([
           'title' => $request->title,
           'description' => $request->description,
           'image' => $imageName,
           'SKU' => bin2hex(random_bytes(4)),
           'price' => $request->price,
           'category_id' => $request->category_id,
        ]);

        return response()->json([
            'product' => $product
        ], 201);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function get(int $id): JsonResponse
    {
        return response()->json([
            'product' => Product::with('category')->find($id)
        ]);
    }

    /**
     * @param UpdateProductRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateProductRequest $request, int $id): JsonResponse
    {
        $imageName = null;
        $productOldValues = Product::find($id);

        if ($request->file('image')) {
            $imageName = $this->imageInStorage->update($request, $productOldValues->image);
        }

        Product::where('id', $id)
            ->update([
                'title' => isset($request->title) ? $request->title : $productOldValues->title,
                'description' => isset($request->description) ? $request->description : $productOldValues->description,
                'price' => isset($request->price) ? $request->price : $productOldValues->price,
                'category_id' => isset($request->category_id) ? $request->category_id : $productOldValues->category_id,
                'image' => !is_null($imageName) ? $imageName : $productOldValues->image,
            ]);

        return response()->json(['message' => 'Product updated successfully']);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $product = Product::find($id);

        if (!is_null($product)) {

            $imagePath = $product->image;
            $this->imageInStorage->delete($imagePath);

            $product->delete();
            return response()->json([], 204);
        }

        return response()->json(['message' => 'Product not found']);
    }
}
