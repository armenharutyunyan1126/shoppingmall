<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'title' => 'Cars',
                'description' => 'Cars description',
            ],
            [
                'title' => 'Houses',
                'description' => 'Houses description',
            ],
            [
                'title' => 'Electronics',
                'description' => 'Electronics description',
            ]
        ]);
    }
}
